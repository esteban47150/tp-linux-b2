# TP2 pt. 1 : Gestion de service

## I. Un premier serveur web

### 1. Installation

#### Installer le serveur Apache

- paquet httpd

    ```
    $ sudo dnf install httpd
    ```
    
#### Démarrer le service Apache

- le service s'appelle httpd
    - démarrez le
    
        ```
        $ sudo systemctl daemon-reload
        $ sudo systemctl start httpd
        ```
    - faites en sorte qu'Apache démarre automatique au démarrage de la machine

        ```
        $ sudo systemctl enable httpd
        ```
        
    - ouvrez le port firewall nécessaire
        - utiliser une commande ss pour savoir sur quel port tourne actuellement Apache
            
            ```
            $ sudo ss -alnpt
            
            State      Recv-Q      Send-Q           Local Address:Port           Peer Address:Port     Process
            LISTEN     0           128                    0.0.0.0:22                  0.0.0.0:*         users:(("sshd",pid=872,fd=5))
            LISTEN     0           128                          *:80                        *:*         users:(("httpd",pid=2116,fd=4), [...]
            LISTEN     0           128                       [::]:22                     [::]:*         users:(("sshd",pid=872,fd=7))
            ```
            
            On en déduit que Apache tourne sur le port 80.
            
#### TESTS

- vérifier que le service est démarré
- vérifier qu'il est configuré pour démarrer automatiquement
    
    ```
    $ sudo systemctl status httpd
    
    httpd.service - The Apache HTTP Server
    Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor     preset: disabled)
    Active: active (running) since Wed 2021-09-29 16:43:30 CEST; 14min ago
    [...]
    ```
    
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement

    ```
    $ curl localhost
    <!doctype html>
    <html>
      <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>HTTP Server Test Page powered by: Rocky Linux</title>
    [...]
            <footer class="col-sm-12">
            <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
            <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
        </footer>
      </body>
    </html>
    ```

- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

    ![](https://gitlab.com/esteban47150/tp-linux-b2/-/raw/main/TP2/images/apache.png)
    
### 2. Avancer vers la maîtrise du service

####  Le service Apache...

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume

    `$ sudo systemctl enable httpd`
    
- prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume

    ```
    $ sudo systemctl is-enabled httpd
    
    enabled
    ```
    
- affichez le contenu du fichier httpd.service qui contient la définition du service Apache

    `$ sudo cat /usr/lib/systemd/system/httpd.service`
    ```bash=
    # See httpd.service(8) for more information on using the httpd service.

    # Modifying this file in-place is not recommended, because changes
    # will be overwritten during package upgrades.  To customize the
    # behaviour, run "systemctl edit httpd" to create an override unit.

    # For example, to pass additional options (such as -D definitions) to
    # the httpd binary at startup, create an override unit (as is done by
    # systemctl edit) and enter the following:

    #       [Service]
    #       Environment=OPTIONS=-DMY_DEFINE

    [Unit]
    Description=The Apache HTTP Server
    Wants=httpd-init.service
    After=network.target remote-fs.target nss-lookup.target httpd-init.service
    Documentation=man:httpd.service(8)

    [Service]
    Type=notify
    Environment=LANG=C

    ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
    ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
    # Send SIGWINCH for graceful stop
    KillSignal=SIGWINCH
    KillMode=mixed
    PrivateTmp=true

    [Install]
    WantedBy=multi-user.target
    ```

#### Déterminer sous quel utilisateur tourne le processus Apache

- mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé

    `$ sudo nano /etc/httpd/conf/httpd.conf`
    ```bash=
    ServerRoot "/etc/httpd"

    Listen 80

    Include conf.modules.d/*.conf

    User apache
    Group apache
    ```
    > 7 | User apache

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

    ```
    $ ps -ef
    
    [...]
    apache      2113    2112  0 16:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache      2114    2112  0 16:43 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
    apache      2115    2112  0 16:43 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
    apache      2116    2112  0 16:43 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
    [...]
    ```
    
- vérifiez avec un ls -al le dossier du site (dans /var/www/...)

    - vérifiez que tout son contenu appartient à l'utilisateur mentionné dans le fichier de conf

        ```
        $ ls -al /var/www

        total 4
        drwxr-xr-x.  4 root root   33 Sep 29 16:36 .
        drwxr-xr-x. 22 root root 4096 Sep 29 16:36 ..
        drwxr-xr-x.  2 root root    6 Jun 11 17:35 cgi-bin
        drwxr-xr-x.  2 root root    6 Jun 11 17:35 html
        ```
        
#### Changer l'utilisateur utilisé par Apache

- créez le nouvel utilisateur
    - pour les options de création, inspirez-vous de l'utilisateur Apache existant
        - le fichier /etc/passwd contient les informations relatives aux utilisateurs existants sur la machine
        - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut

            ```sh
            $ sudo useradd web
            ```
            
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

    `$ sudo nano /etc/httpd/conf/httpd.conf`
    ```bash=
    ServerRoot "/etc/httpd"

    Listen 80

    Include conf.modules.d/*.conf

    User web
    Group web
    ```
    
- redémarrez Apache

    ```
    $ sudo systemctl stop httpd
    $ sudo systemctl start httpd
    ```
    
- utilisez une commande `ps` pour vérifier que le changement a pris effet

    ```
    $ ps -ef

    [...]
    web         1790    1789  0 20:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
	web         1791    1789  0 20:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
	web         1792    1789  0 20:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
	web         1793    1789  0 20:51 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    ```
    
#### Faites en sorte que Apache tourne sur un autre port

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix

    `$ sudo nano /etc/httpd/conf/httpd.conf`
    ```bash=
    ServerRoot "/etc/httpd"

    Listen 6969
    [...]
    ```
    
- ouvrez un nouveau port firewall, et fermez l'ancien

    ```
    $ sudo firewall-cmd --remove-port=80/tcp --permanent
    $ sudo firewall-cmd --add-port=6969/tcp --permanent
    ```
    
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi

    ```
    $ ss -alnpt
    
    [...]
    LISTEN        0        128        *:6969        *:*
    ```
    
- vérifiez avec curl en local que vous pouvez joindre Apache sur le nouveau port

    ```html
    $ curl localhost:6969
    
	<!doctype html>
	<html>
        <head>
            [...]
        </body>
    </html>
    ```
    
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

    Ca fontionne :)
    
[📁 Fichier /etc/httpd/conf/httpd.conf](https://gitlab.com/esteban47150/tp-linux-b2/-/blob/main/TP2/files/httpd.conf)
    
## II. Une stack web plus avancée

### 2. Setup

#### A. Serveur Web et NextCloud

##### Install du serveur Web et de NextCloud sur `web.tp2.linux`

```shell
$ sudo dnf install epel-release
$ sudo dnf update
$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
$ sudo dnf module enable php:remi-8.1
$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
$ sudo systemctl enable httpd
$ sudo mkdir /etc/httpd/sites-available
$ sudo mkdir /etc/httpd/sites-enabled
$ sudo vi /etc/httpd/sites-available/web.tp2.linux
```
```bash=
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/web.tp2.linux/html/
  ServerName  web.tp2.linux

  <Directory /var/www/sub-domains/web.tp2.linux/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
```shell
$ sudo nano /etc/httpd/conf/httpd.conf
$ sudo ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/
$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html
$ sudo vi /etc/opt/remi/php74/php.ini
```
```=
[...]
;date.timezone = "Europe/Paris"
[...]
```
```shell
$ sudo wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
$ sudo unzip nextcloud-22.2.0.zip
$ cd nextcloud
$ sudo mv * /var/www/sub-domains/web.tp2.linux/html/
$ chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/html
$ sudo systemctl restart httpd
$ sudo firewall-cmd --add-port=80/tcp --permanent
$ sudo firewall-cmd --reload
```

[📁 Fichier /etc/httpd/conf/httpd.conf](https://gitlab.com/esteban47150/tp-linux-b2/-/blob/main/TP2/files/httpdNEXTCLOUD.conf)
[📁 Fichier /etc/httpd/sites-available/web.tp2.linux](https://gitlab.com/esteban47150/tp-linux-b2/-/blob/main/TP2/files/web.tp2.linux)

#### B. Base de données

##### Install de MariaDB sur `db.tp2.linux`

- déroulez la doc d'install de Rocky
- je veux dans le rendu toutes les commandes réalisées

```
$ sudo dnf install mariadb-server
$ sudo systemctl enable mariadb
$ sudo systemctl start mariadb
$ sudo mysql_secure_installation
```

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

```
$ ss -alnpt

State      Recv-Q     Send-Q         Local Address:Port         Peer Address:Port     Process
LISTEN     0          128                  0.0.0.0:22                0.0.0.0:*
LISTEN     0          128                     [::]:22                   [::]:*
LISTEN     0          80                         *:3306                    *:*
```

> Le port 22 correspond a la connexion SSH, on peut en déduire que mariadb utilise le port 3306.

##### Préparation de la base pour NextCloud

- une fois en place, il va falloir préparer une base de données pour NextCloud : 
    - connectez-vous à la base de données à l'aide de la commande sudo mysql -u root -p
    - exécutez les commandes SQL suivantes : 
        ```sql
        # Création d'un utilisateur dans la base, avec un mot de passe
        # L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
        # Dans notre cas, c'est l'IP de web.tp2.linux
        # "meow" c'est le mot de passe :D
        CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';

        # Création de la base de donnée qui sera utilisée par NextCloud
        CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

        # On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
        GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

        # Actualisation des privilèges
        FLUSH PRIVILEGES;
        ```
        
##### Exploration de la base de données

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera : 
    - depuis la machine web.tp2.linux vers l'IP de db.tp2.linux
    - vous pouvez utiliser la commande mysql pour vous connecter à une base de données depuis la ligne de commande
        - par exemple mysql -u \<USER> -h <IP_DATABASE> -p

            ```
            $ mysql -u nextcloud -h 10.102.1.12 -p
            ```
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

    ```sql
    SHOW DATABASES;
    USE <DATABASE_NAME>;
    SHOW TABLES;
    ```

- trouver une commande qui permet de lister tous les utilisateurs de la base de données

    ```mysql
    SELECT User FROM mysql.user;

    +-----------+
    | User      |
    +-----------+
    | nextcloud |
    | root      |
    | root      |
    | root      |
    +-----------+
    ```        

#### C. Finaliser l'installation de NextCloud
        
#####  sur votre PC

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
    - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
    - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
    - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
    - choisissez "MySQL/MariaDB"
    - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

##### Exploration de la base de données

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
    - bonus points si la réponse à cette question est automatiquement donnée par une requête SQL

        ```mysql
        select table_schema as database_name,
            count(*) as tables
        from information_schema.tables
        where table_type = 'BASE TABLE'
              and table_schema not in ('information_schema', 'sys',
                                       'performance_schema', 'mysql')
        group by table_schema
        order by table_schema;
        
        
        +---------------+--------+
        | database_name | tables |
        +---------------+--------+
        | nextcloud     |     87 |
        +---------------+--------+
        1 row in set (0.003 sec)
        ```
        
---

## Tableau des machines

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | *             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11   |

---
#### Esteban Martinez - B2B

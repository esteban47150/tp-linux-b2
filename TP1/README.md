# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

### Setup de deux machines Rocky Linux configurées de façon basique.

- un accès internet (via la carte NAT)

    ```
    $ ping 8.8.8.8

    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=21.6 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=21.7 ms
    ```

- un accès à un réseau local (les deux machines peuvent se ping) (via la carte Host-Only)

    ```
    $ ping 10.101.1.12

    PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
    64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=5.42 ms
    64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.05 ms
    ```

- les machines doivent avoir un nom

    ```
    sudo nano /etc/hostname
    ```

- utiliser 1.1.1.1 comme serveur DNS

    ```
    $ sudo nano /etc/resolv.conf
    ```
    ```
    nameserver 1.1.1.1
    ```

    - vérifier avec le bon fonctionnement avec la commande dig
    
        - avec `dig`, demander une résolution du nom `ynov.com`

                $ dig ynov.com

                ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
                ;; global options: +cmd
                ;; Got answer:
                ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54416
                ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

                ;; OPT PSEUDOSECTION:
                ; EDNS: version: 0, flags:; udp: 1232
                ;; QUESTION SECTION:
                ;ynov.com.                      IN      A

                ;; ANSWER SECTION:
                ynov.com.               3579    IN      A       92.243.16.143

                ;; Query time: 21 msec
                ;; SERVER: 1.1.1.1#53(1.1.1.1)
                ;; WHEN: Fri Sep 24 15:54:55 CEST 2021
                ;; MSG SIZE  rcvd: 53
                
        - mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé
            
            ```
            ;; ANSWER SECTION:
            ynov.com.    3579    IN    A    92.243.16.143
            ```
        
        - mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu

            ```
            ;; SERVER: 1.1.1.1#53(1.1.1.1)
            ```
- les machines doivent pouvoir se joindre par leurs noms respectifs

    ```
    sudo nano /etc/hosts
    ```
    ```
    10.101.1.12 node1
    ```
    et sur l'autre machine : 
    ```
    10.101.1.11 node1
    ```
    
    - assurez-vous du bon fonctionnement avec des ping \<NOM>
    
        ```
        $ ping node2
        
        PING node2 (10.101.1.12) 56(84) bytes of data.
        64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=2.13 ms
        64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.73 ms
        ```
        ```
        $ ping node1
        
        PING node1 (10.101.1.11) 56(84) bytes of data.
        64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=2.35 ms
        64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.673 ms
        ```
    
## I. Utilisateurs

### 1. Création et configuration

#### Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que : 

- le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home
- le shell de l'utilisateur soit /bin/bash

    ```
    $ sudo useradd user --home /home --shell /bin/bash
    ```

#### Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.

```
$ sudo groupadd admins
```

- il faut modifier le fichier /etc/sudoers

    ```
    $ sudo visudo /etc/sudoers
    ```
    
    On rajoute cette ligne : 
    
    ```bash=
    %admins ALL=(ALL)    ALL
    ```
####  Ajouter votre utilisateur à ce groupe admins.

```
$ sudo usermod -aG admins user2
```

### 2. SSH

#### Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine.

- il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance
    - génération de clé depuis VOTRE poste donc

        ```
        $ ssh-keygen -t rsa -b 4096
        ```

- déposer la clé dans le fichier `/home/\<USER>/.ssh/authorized_keys` de la machine que l'on souhaite administrer

    - on peut le faire à la main
        
        ```
        $ scp C:\Users\Esteban.ESTEBAN-PC\.ssh\id_rsa.pub user2@10.101.1.11:/home/user2/authorized_keys
        ```
        ```
        $ mkdir /home/user2/.ssh
        $ mv authorized_keys /home/user2/.ssh/
        $ chmod 600 authorized_keys
        $ cd ..
        $ chmod 700 .ssh
        ```

####  Assurez vous que la connexion SSH est fonctionnelle, sans avoir besoin de mot de passe.

```
> ssh user2@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 16:35:52 2021 from 10.101.1.1
[user2@node1 ~]$ _
```

## II. Partitionnement

### 2. Partitionnement

#### Utilisez LVM pour : 

- agréger les deux disques en un seul volume group

    ```
    $ sudo pvcreate /dev/sdb
    $ sudo pvcreate /dev/sdc
    
    $ sudo vgcreate data /dev/sdb
    
    $ sudo vgextend data /dev/sdc
    ```
    
- créer 3 logical volumes de 1 Go chacun

    ```
    $ sudo lvcreate -L 1G data -n lv1
    $ sudo lvcreate -L 1G data -n lv2
    $ sudo lvcreate -L 1G data -n lv3
    ```

- formater ces partitions en `ext4`

    ```
    $ sudo mkfs -t ext4 /dev/data/lv1
    $ sudo mkfs -t ext4 /dev/data/lv2
    $ sudo mkfs -t ext4 /dev/data/lv3
    ```
    
- monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1, /mnt/part2 `et` /mnt/part3`

    ```
    $ sudo mkdir /mnt/part1
    $ sudo mkdir /mnt/part2
    $ sudo mkdir /mnt/part3

    $ sudo mount /dev/data/lv1 /mnt/part1
    $ sudo mount /dev/data/lv2 /mnt/part2
    $ sudo mount /dev/data/lv3 /mnt/part3
    ```
    
- Grâce au fichier `/etc/fstab`, faites en sorte que cette partition soit montée automatiquement au démarrage du système

    ```
    $ sudo nano /etc/fstab
    ```
    ```bash=
    /dev/data/lv1           /mnt/part1              ext4    defaults        0 0
    /dev/data/lv2           /mnt/part2              ext4    defaults        0 0
    /dev/data/lv3           /mnt/part3              ext4    defaults        0 0
    ```
    ```
    $ sudo umount /mnt/part1
    $ sudo umount /mnt/part2
    $ sudo umount /mnt/part3

    $ sudo mount -av
    /                        : ignored
    /boot                    : already mounted
    none                     : ignored
    mount: /mnt/part1 does not contain SELinux labels.
           You just mounted an file system that supports labels which does not
           contain labels, onto an SELinux box. It is likely that confined
           applications will generate AVC messages and not be allowed access to
           this file system.  For more details see restorecon(8) and mount(8).
    /mnt/part1               : successfully mounted
    mount: /mnt/part2 does not contain SELinux labels.
           You just mounted an file system that supports labels which does not
           contain labels, onto an SELinux box. It is likely that confined
           applications will generate AVC messages and not be allowed access to
           this file system.  For more details see restorecon(8) and mount(8).
    /mnt/part2               : successfully mounted
    mount: /mnt/part3 does not contain SELinux labels.
           You just mounted an file system that supports labels which does not
           contain labels, onto an SELinux box. It is likely that confined
           applications will generate AVC messages and not be allowed access to
           this file system.  For more details see restorecon(8) and mount(8).
    /mnt/part3               : successfully mounted
    ```
    
## III. Gestion de services

### 1. Interaction avec un service existant

#### Assurez-vous que : 

- l'unité est démarrée
- l'unitée est activée (elle se lance automatiquement au démarrage)

    ```
    $ systemctl list-units -t service

    [...]
    firewalld.service                  loaded active running firewalld - dynamic firewall daemon
    [...]
    ```
    
### 2. Création de service

#### A. Unité simpliste

##### Créer un fichier qui définit une unité de service `web.service` dans le répertoire `/etc/systemd/system`

```
$ sudo nano /etc/systemd/system/web.service
```
```bash=
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```
```
$ sudo systemctl daemon-reload

$ sudo systemctl status web
$ sudo systemctl start web
$ sudo systemctl enable web
```
    
##### Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.

```html
$ curl 10.101.1.11:8888

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
[...]
</body>
</html>
```

#### B. Modification de l'unité

##### Créer un utilisateur web

```
$ sudo useradd web
```

##### Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses : 

- `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
- `WorkingDirectory=` afin de lancer le serveur depuis un dossier spécifique, choisissez un dossier que vous avez créé dans `/srv`
- ces deux clauses sont à positionner dans la section `[Service]` de votre unité

```
sudo nano /etc/systemd/system/web.service
```
```bash=
User=web
WorkingDirectory=/srv/web
```

##### Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.

```
$ sudo mkdir /srv/web
$ sudo nano /srv/web/fichier

$ sudo systemctl daemon-reload

$ sudo chown web /srv
$ sudo chown web /srv/web
$ sudo chown web /srv/web/fichier
```

#####  Vérifier le bon fonctionnement avec une commande curl

```
$ curl 10.101.1.11:8888

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
[...]
<li><a href="srv/">srv/</a></li>
[...]
```

---
#### Esteban Martinez - B2B
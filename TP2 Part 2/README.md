# TP2 pt. 2 : Maintien en condition opérationnelle

## I. Monitoring

### 2. Setup

#### Setup Netdata

- on va aller au plus simple, exécutez, sur toutes les machines que vous souhaitez monitorer : 

    ```bash
    $ sudo su -
    $ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
    $ exit
    ```
#### Manipulation du *service* Netdata

- un *service* `netdata` a été créé
- déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine

    ```
    $ sudo systemctl status netdata
    
    ● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:23:08 CEST; 9min ago
   [...]
    ```
    > enabled;
    
    - si ce n'est pas le cas, faites en sorte qu'il démarre au boot de la machine
    
        ```
        $ sudo systemctl enable netdata
        ```
        
- déterminer à l'aide d'une commande `ss` sur quel port Netdata écoute

```
$ ss -alnpt

State      Recv-Q     Send-Q          Local Address:Port            Peer Address:Port     Process
LISTEN     0          128                 127.0.0.1:8125                 0.0.0.0:*
LISTEN     0          128                   0.0.0.0:19999                0.0.0.0:*
LISTEN     0          128                   0.0.0.0:22                   0.0.0.0:*
LISTEN     0          128                     [::1]:8125                    [::]:*
LISTEN     0          128                      [::]:19999                   [::]:*
LISTEN     0          128                         *:80                         *:*
LISTEN     0          128                      [::]:22                      [::]:*
```

> netdata écoute sur le port 19999

- autoriser ce port dans le firewall

```
$ sudo firewall-cmd --add-port=19999/tcp --permanent
$ sudo firewall-cmd --reload
```

#### Setup Alerting

- ajustez la conf de Netdata pour mettre en place des alertes Discord

    ```bash
    $ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
    ```

    ``` ###############################################################################
    # sending discord notifications

    # note: multiple recipients can be given like this:
    #                  "CHANNEL1 CHANNEL2 ..."

    # enable/disable sending discord notifications
    SEND_DISCORD="YES"

    # Create a webhook by following the official documentation -
    # https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
    DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897120657316380682/1eLdyK8ILhGkfCwyQZPQRVFZhAJHs555bqrygmsoe4xy9ONgnkL8xVENl9vrn7dLCqhv"

    # if a role's recipients are not configured, a notification will be send to
    # this discord channel (empty = do not send a notification for unconfigured
    # roles):
    DEFAULT_RECIPIENT_DISCORD="alerte-sql"
    ```

- vérifiez le bon fonctionnement de l'alerting sur Discord

    ```bash
    # become user netdata
    su -s /bin/bash netdata

    # enable debugging info on the console
    export NETDATA_ALARM_NOTIFY_DEBUG=1

    # send test alarms to sysadmin
    /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
    ```
    
    ```bash
    $ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
    ```
    
#### Config alerting

- créez une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM

    ```bash
    $ sudo nano /opt/netdata/etc/netdata/health.d/ramAlert.conf
    ```
    ```bash=
     alarm: ram_usage
     on: system.ram
    lookup: average -1m percentage of used
     units: %
     every: 1m
     warn: $this > 50
     crit: $this > 80
     info: The percentage of RAM used by the system.
    ```
    ```bash
    $ sudo killall -USR2 netdata
    $ sudo systemctl restart netdata
    ```
- testez que votre alerte fonctionne
    - il faudra remplir artificiellement la RAM pour voir si l'alerte remonte correctement
    - sur Linux, on utilise la commande `stress` pour ça
    - soyez patient, et laissez durer le stress test (pas la journée non plus)

        ```
        $ stress --vm 1 --vm-bytes 250M -t 90s -v
        ```
        
        J'ai bien reçu les alertes dans le channel discord.
        
## II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées|
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80          | *             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 19999       | 10.102.1.11   |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | web & db      |

### 2 . Partage NFS

#### Setup environnement

- créer un dossier ``/srv/backups/``

    ```
    backup$ sudo mkdir /srv/backups/
    ```

- il contiendra un sous-dossier pour chaque machine du parc
    - commencez donc par créer le dossier `/srv/backups/web.tp2.linux/`

        ```
        backup$ sudo mkdir /srv/backups/web.tp2.linux/
        ```
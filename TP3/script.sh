#!/bin/bash
# Minecraft Server auto install
# Esteban ~ 28/10/21

# Colors
N="\e[0m"
B="\e[1m"
G="\e[32m"
R="\e[31m"
Y="\e[33m"

# Check admin privileges
check_root() {
  if [ $UID -ne 0 ]; then
    echo "${R}[ERROR] ${N}This program need administrator privileges to run. (try \$sudo !!)"
    exit 1
  fi
}

# Check admin privileges
check_root
clear

# Update system
sudo dnf check-update

# Create user
sudo useradd client

# Create server directory
sudo mkdir minecraft
cd minecraft

# Install java runtime
sudo dnf install -y java-1.8.0-openjdk-devel

# Create Minecraft server
sudo curl -o server.jar https://launcher.mojang.com/v1/objects/886945bfb2b978778c3a0288fd7fab09d315b25f/server.jar

# Launch server to create 'eula.txt' file
java -Xmx512M -Xms2048M -jar server.jar nogui
sed -i -e 's/eula=false/eula=true/g' eula.txt

# Set permission
sudo chmod 100 server.jar

# Allow connections on port 25565
sudo firewall-cmd --add-port=25565/udp --permanent
sudo firewall-cmd --reload
clear

# Change user
su client

# Finally launch the server
echo ${G} Server starting ... ${N}
java -Xmx512M -Xms2048M -jar server.jar nogui
